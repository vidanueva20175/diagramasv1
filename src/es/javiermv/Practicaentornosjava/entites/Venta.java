/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.javiermv.Practicaentornosjava.entites;


public class Venta {
    private Comprador c;
    private Motos m;
    private Vendedor a;

    public Comprador getC() {
        return c;
    }

    public void setC(Comprador c) {
        this.c = c;
    }

    public Motos getM() {
        return m;
    }

    public void setM(Motos m) {
        this.m = m;
    }

    public Vendedor getA() {
        return a;
    }

    public void setA(Vendedor a) {
        this.a = a;
    }

    public Venta(Comprador c, Motos m, Vendedor a) {
        this.c = c;
        this.m = m;
        this.a = a;
    }

    @Override
    public String toString() {
        return "El comprador" + c.toString() + m.toString() + ", siendo atendido por " + a.toString();
    }

    
}
