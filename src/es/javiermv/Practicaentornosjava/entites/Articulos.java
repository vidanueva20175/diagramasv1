/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.javiermv.Practicaentornosjava.entites;

/**
 *
 * @author alumno
 */
public abstract class Articulos {
    
    private String cod_producto;

    public Articulos(String cod_producto) {
        this.setCod_producto(cod_producto);
    }

    public String getCod_producto() {
        return cod_producto;
    }

    public void setCod_producto(String cod_producto) {
        this.cod_producto = cod_producto;
    }

    @Override
    public String toString() {
        return " ha comprado un producto con código  " + cod_producto;
    }
    
}

    
    

