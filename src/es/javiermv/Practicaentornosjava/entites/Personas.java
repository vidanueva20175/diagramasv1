/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.javiermv.Practicaentornosjava.entites;

/**
 *
 * @author alumno
 */
public  abstract class Personas {

   
    private String nombre, apellidos;

    public Personas(String nombre, String apellidos) {
        this.setNombre(nombre);
        this.setApellidos(apellidos);
      
    }  

    public Personas() {
    }
    

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

 
     @Override
    public String toString() {
        return  " "+ nombre + " " + apellidos ;
    }
    
}
