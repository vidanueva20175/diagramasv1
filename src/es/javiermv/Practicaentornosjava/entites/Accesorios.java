/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.javiermv.Practicaentornosjava.entites;

/**
 *
 * @author alumno
 */
public class Accesorios extends Articulos{
    private String nombre,marca,modelo;
    private int precio;

    public Accesorios(String nombre, String marca, String modelo, int precio, String cod_producto) {
        super(cod_producto);
        this.setNombre(nombre);
        this.setMarca(marca);
        this.setModelo(modelo);
        this.setPrecio(precio);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

    @Override
    public String toString() {
        return super.toString() + nombre + " con  marca=" + marca + " , modelo=" + modelo + "tiene un  precio=" + precio ;
    }


}

   

   