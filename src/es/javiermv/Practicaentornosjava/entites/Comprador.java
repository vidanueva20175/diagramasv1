/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.javiermv.Practicaentornosjava.entites;

/**
 *
 * @author alumno
 */
public class Comprador extends Personas {
   private String direccion,dni;

    public Comprador(String direccion, String dni, String nombre, String apellidos) {
        super(nombre, apellidos);
        this.setDireccion(direccion);
        this.setDni(dni);
       
    }
   
   
   

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }
  

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

  

    @Override
    public String toString() {
        return super.toString()+" con domicilio en " + direccion + " y dni " + dni;
    }
   
   
   
    
    
    
}
