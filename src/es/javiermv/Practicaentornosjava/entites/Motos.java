/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.javiermv.Practicaentornosjava.entites;

/**
 *
 * @author alumno
 */
public class Motos extends Articulos {
   
    private String marca,modelo;
    private int cilindrada;
    private int precio;

    public Motos(String marca, String modelo, int cilindrada, int precio, String cod_producto) {
        super(cod_producto);
        this.setMarca(marca);
        this.setModelo(modelo);
        this.setCilindrada(cilindrada);
        this.setPrecio(precio);
    }

   

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public int getCilindrada() {
        return cilindrada;
    }

    public void setCilindrada(int cilindrada) {
        this.cilindrada = cilindrada;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }
    
 @Override
    public String toString() { 
        return super.toString()+" marca " + marca + ", modelo " + modelo + " de " + cilindrada + " centimetros cúbicos , y le ha costado  " + precio + " euros";
    }   
    
    
}
