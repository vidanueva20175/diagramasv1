/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.javiermv.Practicaentornosjava.entites;

/**
 *
 * @author alumno
 */
public class Vendedor extends Personas{
    private int cod_vendedor;

    public Vendedor(int cod_vendedor, String nombre, String apellidos) {
        super(nombre, apellidos);
        this.setCod_vendedor(cod_vendedor);
    }

    public Vendedor() {
    }

 
    

    public int getCod_vendedor() {
        return cod_vendedor;
    }

    public void setCod_vendedor(int cod_vendedor) {
        this.cod_vendedor = cod_vendedor;
    }

    @Override
    public String toString() {
        return super.toString() + " con codigo de vendedor " + cod_vendedor ;
    }
    
    
    
    
}
